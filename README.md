# Study Guacamole and RDP
This codebase contains a docker-compose file to study guacamole configuration for linux rdp servers.

```
docker-compose up
# localhost:8080
# login with : guacadmin, guacadmin
# go to option>connection and add new connection
# ssh-> user: abc, password: abc, ip: openssh-server, port: 2222

# [FAILED] RDP-> user: abc, password: abc, ip: rdesktop, port: 3389
```

# Study
* docker, docker compose
* guacamole
* [rdesktop](https://hub.docker.com/r/linuxserver/rdesktop): remote desktop container 
* [openssh server](https://hub.docker.com/r/linuxserver/openssh-server): remote ssh server
  * But I also try [danchitnis/xrdp](https://hub.docker.com/r/danchitnis/xrdp)
* [guacamole](https://github.com/flcontainers/guacamole): the all in one container for guacamole (to not setup postgres and guac backend and guacamole frontend in different containers)